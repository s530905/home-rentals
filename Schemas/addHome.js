var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var addHomeSchema = new Schema({
    houseNumber: Number,
    address: String,
    amount: Number
});

module.exports = mongoose.model('addHome', addHomeSchema);