const express = require('express')
const router = express.Router()



router.get('/', function(req, res)  {
    res.render('login.ejs')
  });
  
router.get('/login', function(req, res) {
    res.render('login.ejs')
  });

router.get('/contactForm', function(req,res){
  console.log('entered');
  res.render('contactForm.ejs');
})

router.get('/addHome', function(req, res) {
    res.render('addHome.ejs')
  });

router.get('/home', function(req, res) {
    res.render('home.ejs')
  });

router.get('/signup', function(req, res) {
    res.render('signup.ejs');
  });

router.get('/admin', function(req, res) {
    res.render('admin.ejs');
  });
  
router.get('/rentalPortal', function(req, res) {
  res.render('rentalPortal.ejs');
});
router.get('/adminHomeRental', function(req, res) {
  res.render('adminHomeRental.ejs');
});
router.get('/updateHouse', function(req, res) {
  res.render('updateHouse.ejs');
});
router.get('/paymentHistory', function(req, res) {
  res.render('paymentHistory.ejs');
});
router.get('/payment', function(req, res) {
  res.render('payment.ejs');
});
router.all('/previousPayments', function(req, res) {
  res.render('previousPayments.ejs');
});
router.get('/paymentsucess', function(req, res) {
  res.render('paymentsucess.ejs');
});

router.get('*', function(req, res) {
    res.render('error.ejs');
  });

module.exports = router